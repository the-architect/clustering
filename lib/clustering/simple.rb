module Clustering

  class Simple

    # input: "data" should be an array of arrays
    # each array should contain elements that are connected to each other
    # elements are counted as often as they occur in the arrays
    # this class does not make sure to only have unique elements in an array
    # [
    #   [1,2,3],
    #   [2,3],
    #   [3,1]
    # ]
    def initialize(data)
      @original = data
    end

    # create a hash of hashes which contains the grade of linkage between elements
    # connections[3][4] #=> 2
    def connections
      @connections ||= @original.inject(Hash.new{|h,k| h[k] = Hash.new(0)}) do |akk, e|
        e.each do |key|
          e.each do |id|
            akk[key][id] += 1 unless key == id
          end
        end
        akk
      end
    end

    def relations
      @relations ||= connections.inject(Hash.new{|h,k| h[k] = Hash.new{|h,k| h[k] = Array.new }}) do |akk, e|
        e.last.each do |k,v|
          akk[e.first][v].push k
        end
        akk
      end
    end

    def clusters(threshold = 2)
      relations.inject(Hash.new{|h,k| h[k] = Array.new }) do |akk, e|
        key, values = *e
        keys = values.keys.select{|s| s >= threshold}.sort.reverse
        keys.each do |k|
          akk[key].concat values[k]
        end
        akk
      end
    end

    def strength?(a, b)
      connections[a][b]
    end

  end

end
