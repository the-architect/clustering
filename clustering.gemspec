# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'clustering/version'

Gem::Specification.new do |gem|
  gem.name          = "clustering"
  gem.version       = Clustering::VERSION
  gem.authors       = ["the-architect"]
  gem.email         = ["marcel.scherf@epicteams.com"]
  gem.description   = %q{Cluster elements that are connected}
  gem.summary       = %q{Cluster elements}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
end
