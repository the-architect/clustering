require_relative '../../spec_helper'

describe Clustering::Simple do

  subject{ Clustering::Simple.new(TEST_DATA.clone.freeze) }

  it 'should find count connections from data' do
    connections = subject.connections
    connections.keys.should_not be_empty

    connections[3][4].should eql 2
  end

  it 'should find strength between elements' do
    subject.strength?(3,4).should eql 2
  end

  it 'should build relations and put them in strength buckets' do
    relations = subject.relations
    relations[3][2].should eql [1,4]
  end

  it 'should find clusters' do
    clusters = subject.clusters
    clusters.keys.sort.should eql [1,3,4]
    clusters[1].should eql [3]
    clusters[3].should eql [1,4]
    clusters[4].should eql [3]
  end

end