require 'rspec/autorun'

RSpec.configure do |config|
  #config.mock_with :mocha
  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.filter_run_excluding :wip => true
end

require_relative '../lib/clustering'

# these ids are related to each other
# they could represent tag-ids for each user profile
TEST_DATA = [
  [1,3,4,5],
  [2,3,4],
  [1,3,8,7]
]
